from django_filters import rest_framework as filters

from common.filters import CharInFilter, NumberInFilter


class VacancyFilter(filters.FilterSet):
    state = CharInFilter()
    owner = NumberInFilter()
