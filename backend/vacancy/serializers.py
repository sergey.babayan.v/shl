from rest_framework import serializers
from .models import Vacancy, CandidateAbilityTest


class VacancySerializer(serializers.ModelSerializer):
    free = serializers.BooleanField(read_only=True)
    mine = serializers.BooleanField(read_only=True)

    class Meta:
        model = Vacancy
        exclude = ()


class VacancySyncSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    owner = serializers.IntegerField(
        source="foreign_owner", required=False, allow_null=True
    )

    class Meta:
        model = Vacancy
        fields = ("id", "owner", "state")


class CandidateResponse(serializers.ModelSerializer):
    vacancy_id = serializers.IntegerField(source="vacancy")

    class Meta:
        model = CandidateAbilityTest
        exclude = ("vacancy",)
