import logging
from celery import shared_task
import requests
from django.conf import settings

from .models import Vacancy, CandidateAbilityTest
from .serializers import VacancySyncSerializer, CandidateResponse

logger = logging.getLogger(__name__)


@shared_task
def sync_with_service():
    response = requests.get(settings.SERVICE_A_URL)
    serializer = VacancySyncSerializer(data=response.json(), many=True)
    if serializer.is_valid():
        logger.info("Обработка полученных данных")
        Vacancy.service.bulk_sync(serializer.validated_data)
        return
    logger.error("Произошла ошибка при синхронизации")


@shared_task()
def sync_responses():
    response = requests.get(settings.SERVICE_A_URL)
    serializer = CandidateResponse(data=response.json(), many=True)
    if serializer.is_valid():
        logger.info("Обработка полученных откликов")
        CandidateAbilityTest.service.bulk_create(serializer.validated_data)
        return
    logger.error("Произошла ошибка при записи откликов")
