from django.core.management import BaseCommand

from vacancy.factories import VacancyFactory


class Command(BaseCommand):
    help = "Init db with fake Vacancies"

    def handle(self, *args, **options):
        VacancyFactory.create_batch(25)
        self.stdout.write(self.style.SUCCESS("Вакансии добавлены"))
