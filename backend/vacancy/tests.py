from django.test import TestCase

# Create your tests here.
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from vacancy.factories import VacancyFactory
from vacancy.models import Vacancy
from vacancy.serializers import VacancySyncSerializer


class TestVacancyRetrieve(APITestCase):
    list_url = "vacancy-list"
    create_count = 5

    def setUp(self):
        VacancyFactory.create_batch(self.create_count)

    def test_retrieve_active_count(self):
        count = Vacancy.objects.active().count()
        path = reverse(self.list_url)
        response = self.client.get(path, data={"state": Vacancy.STATE_ACTIVE})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), count)

    def test_retrieve_archived_count(self):
        count = Vacancy.objects.archived().count()
        path = reverse(self.list_url)
        response = self.client.get(path, data={"state": Vacancy.STATE_ARCHIVE})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), count)


class TestVacancySync(TestCase):
    create_count = 10
    serializer_class = VacancySyncSerializer

    def setUp(self):
        VacancyFactory.create_batch(self.create_count)

    def test_creat_on_empty(self):
        count = Vacancy.objects.count()
        data = self.serializer_class(instance=Vacancy.objects.all(), many=True).data
        Vacancy.objects.all().delete()
        serializer = self.serializer_class(data=data, many=True)
        is_valid = serializer.is_valid()
        self.assertEqual(True, is_valid, serializer.errors)
        validated_data = serializer.validated_data
        Vacancy.service.bulk_sync(validated_data)
        self.assertEqual(
            count, Vacancy.objects.count(), "Количество созданных записей не совпадает"
        )

    def test_archiving_on_sync(self):
        old_vacancies = list(Vacancy.objects.all().values_list("id", flat=True))
        old_vacancies_count = Vacancy.objects.count()
        VacancyFactory.create_batch(2)
        data = self.serializer_class(
            instance=Vacancy.objects.exclude(id__in=old_vacancies), many=True
        ).data
        serializer = self.serializer_class(data=data, many=True)
        is_valid = serializer.is_valid()
        self.assertEqual(True, is_valid, serializer.errors)
        validated_data = serializer.validated_data
        Vacancy.service.bulk_sync(validated_data)
        self.assertEqual(
            old_vacancies_count + 2,
            Vacancy.objects.count(),
            "Количество созданных записей не совпадает",
        )
        self.assertEqual(
            Vacancy.objects.filter(
                state=Vacancy.STATE_ARCHIVE, id__in=old_vacancies
            ).count(),
            old_vacancies_count,
        )
