from django.db import models
from django.db.models import Case, When, Value, BooleanField


class VacancyQuerySet(models.QuerySet):
    """Класс отвечающий за query запросы для Vacancy"""

    def active(self):
        return self.filter(state=self.model.STATE_ACTIVE)

    def archived(self):
        return self.filter(state=self.model.STATE_ARCHIVE)

    def by_user(self, user):
        return self.filter(owner=user.id)

    def annotate_free(self):
        return self.annotate(
            free=Case(
                When(owner__isnull=False, then=0),
                When(foreign_owner__isnull=False, then=0),
                default=1,
                output_field=BooleanField(),
            )
        )

    def annotate_mine(self, user):
        return self.annotate(
            mine=Case(
                When(owner=user.id, then=1), default=0, output_field=BooleanField(),
            )
        )


class AbilityTestQuerySet(models.QuerySet):
    def by_vacancy(self, vacancy):
        return self.filter(vacancies__id=vacancy.id).distinct()


class CandidateAbilityTestQuerySet(models.QuerySet):
    pass
