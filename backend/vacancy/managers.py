import logging

from django.db import models

from common.exceptions import ServiceException

logger = logging.getLogger(__name__)


class VacancyService(models.Manager):
    """Класс отвечающий за бизнесс методы для модели Vacancy"""

    def bulk_sync(self, data):
        self.model.objects.all().update(state=self.model.STATE_ARCHIVE)
        for elem in data:
            self.sync_or_create(elem)

    def sync_or_create(self, elem):
        self.update_or_create(elem, id=elem["id"])

    def take(self, instance, user):
        if instance.owner or instance.foreign_owner:
            raise ServiceException(f"Данная вкансия занята:{instance}")
        instance.owner = user
        instance.save()

    def free(self, instance, user):
        if instance.owner != user:
            raise ServiceException(f"Данная вкансия не иожет быть освобождена")
        instance.owner = None
        instance.save()


class AbilityTestService(models.Manager):
    def bind_tests_to_vacancy(self, vacancy, tests):
        for test in tests:
            obj, created = self.update_or_create(test, id=test.get("id", None))
            if created:
                obj.vacancies.add(vacancy)


class CandidateAbilityTestService(models.Manager):
    def create_piece(self, piece):
        from vacancy.models import Vacancy

        vacancy = Vacancy.objects.filter(id=piece.pop("vacancy_id")).first()
        for test in vacancy.abilities:
            self.create({**piece, **{"ability_test": test, "vacancy": vacancy}})

    def bulk_create(self, data):
        for piece in data:
            self.create_piece(piece)
