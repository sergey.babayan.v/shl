# Generated by Django 3.0.2 on 2020-01-27 22:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("vacancy", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="vacancy",
            name="foreign_owner",
            field=models.IntegerField(
                blank=True, null=True, verbose_name="ID владельца"
            ),
        ),
    ]
