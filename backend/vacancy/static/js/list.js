const api = {
    get: () => `/api/vacancy/`,
    filters: () => `/api/vacancy/filter_specs/`,
    filter: function(states, owners) {
        var query = "";
        if (states || owners) {
            query = encodeQueryData({
                "state": states.join(","),
                "owner": owners.join(","),
            });
        }
        var url = this.get();
        return `${url}?${query}`
    },
    take: (id) => `/api/vacancy/${id}/take/`,
    free: (id) => `/api/vacancy/${id}/free/`
};

$(document).on("click", ".take-action", function(e) {
    e.preventDefault();
    var $bttn = $(this);
    var id = $bttn.data("id");
    $.post(api.take(id), {}).then(function(result) {
        alert("Вакансия взята");
        document.location.reload(true);
    })

});

$(document).on("click", ".free-action", function(e) {
    e.preventDefault();
    var $bttn = $(this);
    var id = $bttn.data("id");
    $.post(api.free(id), {}).then(function(result) {
        alert("Вакансия освобождена");
        document.location.reload(true);
    })
});