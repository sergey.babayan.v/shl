function encodeQueryData(data) {
    const ret = [];
    for (let d in data)
        ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
    return ret.join('&');
}


const api = {
    get: () => `/api/vacancy/`,
    filters: () => `/api/vacancy/filter_specs/`,
    filter: function(states, owners) {
        var query = "";
        if (states || owners) {
            query = encodeQueryData({
                "state": states.join(","),
                "owner": owners.join(","),
            });
        }
        var url = this.get();
        return `${url}?${query}`
    },
    take: (id) => `/api/vacancy/${id}/take/`,
    free: (id) => `/api/vacancy/${id}/free/`
};


requirejs(["redom"], function(redom) {
    el = redom.el;
    mount = redom.mount;
    setChildren = redom.setChildren;


    class ThRow {
        constructor(data) {
            this.el = el("tr");
            this.update(data);
        }

        update(data) {
            var cells = [];
            data.forEach(function(elem) {
                cells.push(el("th", elem))
            });
            this.el = el("tr", cells);
        }
    }

    class Thead {
        constructor() {
            this.el = el("thead", this.heading = new ThRow(["Наименование", "Владелец", "Статус", "Действие"]));
        }
    }

    class TdRow {
        constructor(data) {
            this.el = el("tr");
            this.update(data);
        }

        update(data) {
            var actions = [];
            if (data.free) {
                actions.push(el("button.btn.btn-primary.take-action", "Взять", {
                    "data-id": data.id
                }))
            }
            if (data.mine) {
                actions.push(el("button.btn.btn-danger.free-action", "Освободить", {
                    "data-id": data.id
                }))
            }
            this.el = el("tr", el("td", data.title), el("td", data.foreign_owner), el("td", data.state), ...actions);
        }
    }

    class Tbody {
        constructor(data) {
            this.el = el("tbody");
            this.update(data);
        }

        update(data) {
            if (data === undefined) {
                return
            }
            var rows = [];
            data.forEach(function(elem) {
                rows.push(new TdRow(elem));
            });
            this.el = el("tbody", rows);
        }
    }

    class VacancyTable {
        constructor(data) {
            this.el = el("table.table", this.thead = new Thead(), this.tbody = new Tbody(data));
        }

        update(data) {
            this.tbody.update(data);
        }
    }


    function updateTable(init) {
        var state_data = [{
            id: "ACTIVE"
        }];
        var owner_data = [];
        if (!init) {
            state_data = $('#state-select').select2('data');
            owner_data = $('#owner-select').select2('data');

        }
        $.get(api.filter(state_data.map(elem => elem.id), owner_data.map(elem => elem.id))).then(function(result) {
            var tableComponent = new VacancyTable(result);
            setChildren(column, tableComponent);
        });
    }

    var column = document.getElementById("table-column");
    var tableComponent = new VacancyTable();
    mount(column, tableComponent);
    $(document).ready(function() {
        updateTable(true);
    });


    $(document).ready(function() {

        var $state_select = $('#state-select');
        var $owner_select = $('#owner-select');


        $state_select.select2({
            width: "100%",
            allowClear: true,
            multiple: true,
            placeholder: "Выберите фильтр",
            ajax: {
                url: api.filters(),
                dataType: 'json',
                processResults: function(data) {
                    return {
                        results: data.states.map(function(tuple) {
                            return {
                                "id": tuple[0],
                                "text": tuple[1],
                            }
                        })
                    }
                }

            }
        });


        $owner_select.select2({
            width: "100%",
            allowClear: true,
            placeholder: "Выберите фильтр",
            data: null,
            multiple: true,
            ajax: {
                url: api.filters(),
                dataType: 'json',
                processResults: function(data) {
                    return {
                        results: data.owners
                    }
                }

            }
        });

        $owner_select.on("change", function() {
            updateTable();
        });

        $state_select.on("change", function() {
            updateTable();
        });


        $(document).on("click", ".take-action", function(e) {
            e.preventDefault();
            var $bttn = $(this);
            var id = $bttn.data("id");
            $.post(api.take(id), {}).then(function(result) {
                alert("Вакансия взята");
                updateTable();
            })

        });

        $(document).on("click", ".free-action", function(e) {
            e.preventDefault();
            var $bttn = $(this);
            var id = $bttn.data("id");
            $.post(api.free(id), {}).then(function(result) {
                alert("Вакансия освобождена");
                updateTable();
            })
        });

    });

});