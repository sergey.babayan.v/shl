import factory
from django.contrib.auth import get_user_model
from factory import Sequence, SubFactory, django
from factory.fuzzy import FuzzyInteger

from .models import Vacancy

User = get_user_model()


class VacancyFactory(django.DjangoModelFactory):
    title = factory.Sequence(lambda n: "Vacancy %d" % n)
    owner = factory.Iterator([User.objects.all().order_by("?").first(), None])
    state = factory.Iterator(map(lambda x: x[0], Vacancy.STATES))
    foreign_owner = factory.Iterator(
        [None, FuzzyInteger(100, 9999)],
        getter=(lambda e: e if e is None else e.fuzz()),
    )

    class Meta:
        model = Vacancy
