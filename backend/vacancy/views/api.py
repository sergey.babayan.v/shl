from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from django_filters import rest_framework as filters
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework import permissions

from account.serailizers import UserFilterSerializer
from vacancy.serializers import VacancySerializer
from ..models import Vacancy
from ..filters import VacancyFilter

User = get_user_model()


class VacancyViewSet(ReadOnlyModelViewSet):
    serializer_class = VacancySerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = VacancyFilter
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        print(self.request.user.id)
        return Vacancy.objects.all().annotate_free().annotate_mine(self.request.user)

    @action(detail=False, methods=["GET"])
    def filter_specs(self, request):
        return Response(
            {
                "states": Vacancy.STATES,
                "owners": UserFilterSerializer(
                    instance=User.objects.filter(vacancy__isnull=False).distinct(),
                    many=True,
                ).data,
            }
        )

    @action(detail=True, methods=["POST"])
    def take(self, request, pk):
        user = request.user
        instance = get_object_or_404(Vacancy, id=pk)
        Vacancy.service.take(instance, user)
        return Response({"message": "OK"})

    @action(detail=True, methods=["POST"])
    def free(self, request, pk):
        user = request.user
        instance = get_object_or_404(Vacancy, id=pk)
        Vacancy.service.free(instance, user)
        return Response({"message": "OK"})
