from django.contrib.auth import get_user
from django.contrib.auth.mixins import LoginRequiredMixin
from django.forms import formset_factory, modelform_factory, modelformset_factory
from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView, ListView, DetailView

from vacancy.models import Vacancy, AbilityTest


class MainPage(LoginRequiredMixin, TemplateView):
    template_name = "main/homepage.html"


class ListVacancy(LoginRequiredMixin, ListView):
    model = Vacancy
    template_name = "main/list.html"

    def get_queryset(self):
        user = get_user(self.request)
        return self.model.objects.by_user(user)


class DetailVacancy(LoginRequiredMixin, DetailView):
    model = Vacancy
    template_name = "main/detail.html"

    def get_formset(self):
        return modelformset_factory(AbilityTest, exclude=("vacancies",), extra=1)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        formset_class = self.get_formset()
        context["formset"] = formset_class(
            queryset=AbilityTest.objects.by_vacancy(self.get_object()),
        )
        return context

    def post(self, request, pk):
        self.object = self.get_object()
        context = self.get_context_data()
        formset_class = self.get_formset()
        formset = formset_class(request.POST)
        if formset.is_valid() and formset.cleaned_data:
            AbilityTest.service.bind_tests_to_vacancy(self.object, formset.cleaned_data)
        context["formset"] = formset
        return render(request, self.get_template_names(), context)
