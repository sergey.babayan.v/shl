import base64
import hashlib

from django.conf import settings
from django.db import models
from django.urls import reverse

from .querysets import (
    VacancyQuerySet,
    AbilityTestQuerySet,
    CandidateAbilityTestQuerySet,
)
from .managers import VacancyService, AbilityTestService, CandidateAbilityTestService


class Vacancy(models.Model):
    STATE_ACTIVE = "ACTIVE"
    STATE_ARCHIVE = "ARCHIVE"
    STATES = ((STATE_ACTIVE, "Активный"), (STATE_ARCHIVE, "Неактивный"))

    title = models.CharField(max_length=255, verbose_name="Наименование")
    state = models.CharField(choices=STATES, max_length=16, verbose_name="Состояние")
    foreign_owner = models.IntegerField(
        verbose_name="ID владельца", blank=True, null=True
    )
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        verbose_name="Владелец",
        blank=True,
        null=True,
    )
    abilities = models.ManyToManyField(
        "AbilityTest", blank=True, verbose_name="Тесты", related_name="vacancies"
    )
    candidate_tests = models.ManyToManyField(
        "AbilityTest",
        blank=True,
        verbose_name="Тесты кандидатов",
        through="CandidateAbilityTest",
        related_name="+",
    )

    objects = VacancyQuerySet.as_manager()
    service = VacancyService()

    def get_absolute_url(self):
        return reverse("detail", kwargs={"pk": self.id})

    class Meta:
        verbose_name = "Вакансия"
        verbose_name_plural = "Вакансии"
        ordering = ("id",)


class CandidateAbilityTest(models.Model):
    ability_test = models.ForeignKey("AbilityTest", on_delete=models.CASCADE)
    vacancy = models.ForeignKey(Vacancy, on_delete=models.CASCADE)
    candidate_id = models.IntegerField()

    def get_md5_url(self):
        authHash = hashlib.md5()
        temp = "|".join([self.ability_test.slug, self.vacancy.id, self.candidate_id])
        authHash.update(temp)
        escaped_hash = base64.b64encode(authHash.digest())
        return reverse("test", kwargs={"hash": escaped_hash})

    objects = CandidateAbilityTestQuerySet()
    service = CandidateAbilityTestService()


class AbilityTest(models.Model):
    slug = models.SlugField(null=False)
    title = models.CharField(max_length=250)
    description = models.TextField()
    is_active = models.BooleanField(default=True)

    objects = AbilityTestQuerySet.as_manager()
    service = AbilityTestService()
