from django.urls import include, path
from django.views.generic import TemplateView
from rest_framework.routers import DefaultRouter

from vacancy.views.pages import MainPage, ListVacancy, DetailVacancy
from .views import api

router = DefaultRouter()
router.register("vacancy", api.VacancyViewSet, basename="vacancy")

urlpatterns = [
    path("", MainPage.as_view(), name="main"),
    path("list/", ListVacancy.as_view(), name="list"),
    path("detail/<int:pk>/", DetailVacancy.as_view(), name="detail"),
    path("api/", include(router.urls)),
    path("test/<slug:hash>/", TemplateView.as_view(), name="test"),
]
