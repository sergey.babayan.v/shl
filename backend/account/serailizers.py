from django.conf import settings
from django.contrib.auth import get_user_model
from rest_framework import serializers

User = get_user_model()


class UserFilterSerializer(serializers.ModelSerializer):
    text = serializers.CharField(source="username")

    class Meta:
        model = User
        fields = ("id", "text")
