from rest_framework.exceptions import APIException


class ServiceException(APIException):
    pass
